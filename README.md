Servlet Extension Mapping Showcase
===================================
This tiny web project illustrates how Apache Tomcat handles extension mappings
that include dots in the extension configured in web.xml.

Chapter SRV.11.2 of the [servlet specification][1] defines...

> A string beginning with a `*.` prefix is used as an extension mapping.

So, this tells us, the url-pattern `*.jsp` forwards all requests ending with
`.jsp` to the mapped servlet or filtes. With this, one might also assume that
the url-pattern `*.my.txt` forwards all requests ending with `my.txt` to the
configured servlet or filters. But the latter does not work with Tomcat while
it does work with Jetty.

Tested with

  * _Apache Tomcat 7.0.53:_ does NOT work
  * _Apache Tomcat 8.0.5:_ does NOT work
  * _Jetty 9.1.4.v20140401:_ works

This has been reported as [bug for Tomcat 7][2].

[1]: http://download.oracle.com/otndocs/jcp/servlet-2.5-mrel2-eval-oth-JSpec/
[2]: https://issues.apache.org/bugzilla/show_bug.cgi?id=56430


Try it yourself
----------------
Build this project using Maven:

    mvn clean package

This will create a .war-file in the target folder. Copy it to your favourite
servlet container and check out the following links with your host and context
URL:

  * `/myServlet`
  * `/test.txt`
  * `/test.my.html`

For example: `http://127.0.0.1:8080/servlet-test/test.my.html`

In all three cases `MyServlet` should respond a text file and both, the servlet
and the filter, should write some stuff to standard-out via `log4j`. But: If
you are using Tomcat, the third case does not work.

