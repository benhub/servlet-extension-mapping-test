package com.benjaminplocek.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author <a href="mailto:code@benjaminplocek.com">Benjamin Plocek</a>
 */
public class MyFilter implements Filter {

	private static final Logger log = LoggerFactory.getLogger(MyFilter.class);

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
		log.info("MyFilter.doFilter invoked");
		HttpServletRequest req = (HttpServletRequest) servletRequest;
		log.info("MyFilter.doFilter invoked for " + req.getRequestURI());
		filterChain.doFilter(servletRequest, servletResponse);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		log.info("MyFilter initialized");
	}

	@Override
	public void destroy() {
		log.info("MyFilter destroyed");
	}
}
