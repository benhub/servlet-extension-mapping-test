package com.benjaminplocek.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author <a href="mailto:code@benjaminplocek.com">Benjamin Plocek</a>
 */
public class MyServlet extends HttpServlet {

	private static final Logger log = LoggerFactory.getLogger(MyServlet.class);

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.info("MyServlet invoked for " + req.getRequestURI());
		resp.setHeader("Content-Type", "text/plain");
		PrintWriter out = resp.getWriter();
		out.println("Wohoo!");
		out.close();
	}
}
